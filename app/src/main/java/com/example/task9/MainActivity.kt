package com.example.task9

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random
import kotlin.random.nextInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        generateRandomImage()
    }
    private fun generateRandomImage(){
        val button = findViewById<Button>(R.id.btn)
        val image = findViewById<ImageView>(R.id.image)

        button.setOnClickListener {
            when(Random.nextInt(1..7)){
                1 -> image.setImageResource(R.drawable.frog1)
                2 -> image.setImageResource(R.drawable.frog2)
                3 -> image.setImageResource(R.drawable.frog3)
                4 -> image.setImageResource(R.drawable.frog4)
                5 -> image.setImageResource(R.drawable.frog5)
                6 -> image.setImageResource(R.drawable.frog6)
                7 -> image.setImageResource(R.drawable.frog7)
            }
            button.setBackgroundColor(Color.argb(255, Random.nextInt(256), Random.nextInt(256), Random.nextInt(256),))
        }
    }
}